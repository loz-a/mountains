require([

    'dojo/query',
    'dojo/dom-class',
    'dojo/on',
    'dojo/json',
    'dojo/hash',
    'dojo/topic',
    'put-selector',
    'dojo/_base/config',
    'dojo/text!/../media/contents.json',
    'dojo/NodeList-traverse'

], function(query, domClass, on, JSON, hash, topic, put, config, routesData) { 'use strict';

    var currentCatalogRoute     = { } ;
    var currentSlideNode        = document.getElementById('current-route');
    var sliderNode              = query('.slider', currentSlideNode)[0];

    var teaserNode              = query('.teaser')[0];
    var teaserHeaderNode        = query('h2 a', teaserNode)[0];
    var teaserTextNode          = query('p', teaserNode)[0];

    var teaserGalleryNode       = query('.gallery', currentSlideNode)[0];
    var teaserGalleryListNode   = query('ul', teaserGalleryNode)[0];
    var teaserGalleryHeaderNode = query('h2 a', teaserGalleryNode)[0];

    var navigationNode          = query('.navigation', currentSlideNode)[0];
    var nextBtn                 = query('.next', navigationNode)[0];
    var prevBtn                 = query('.prev', navigationNode)[0];

    var langsNodeList           = query('.langs a');

    routesData = (JSON.parse(routesData))[config.locale];

    topic.subscribe("/dojo/hashchange", viewRoute);

    (function() {
        var h = hash();

        if (h) {
            topic.publish("/dojo/hashchange", h);
        } else if (routesData.length) {
            h = routesData[0].url.split('/').pop();
            setCurrentRoute(h);
            setNavigation();
        }

    })();



    on(currentSlideNode, '.prev:click, .next:click', function(evt) {
        evt.preventDefault();

        var url = this.getAttribute('href').split('#').pop();
        if (!url) return;

        hash(url);
    });


    function viewRoute(url) {
        setCurrentRoute(url);

        domClass.add(navigationNode, 'hidden');

        setLanguageHash();
        setSliderImage();
        setTeaser();
        setGallery();
        setNavigation();

        setTimeout(function() {
            domClass.replace(navigationNode, 'fadeIn', 'hidden');
        }, 1000);
    } // viewNextRoute()


    function setLanguageHash() {
        langsNodeList.forEach(function(node) {
            var href = node.getAttribute('href');
            var idx = href.indexOf('#');

            if (~idx)  href = href.substring(0, idx);

            node.setAttribute('href', href + location.hash);
        });

    } // setLanguageHash()


    function setSliderImage() {
        var newImageNode = document.createElement('img');
        var imgData = currentCatalogRoute.data.images[0];

        newImageNode.src = {}.toString.call(imgData) === '[object Object]' ? imgData.src : imgData;

        var currentSlideImage = query('img', sliderNode)[0];
        sliderNode.insertBefore(newImageNode, currentSlideImage);

        currentSlideImage.className = 'animated fadeOut';

        setTimeout(function() {
            currentSlideImage.parentNode.removeChild(currentSlideImage);
        }, 3000);
    } // setBackgroundImage()


    function setTeaser() {
        var route = currentCatalogRoute.data;
        domClass.replace(teaserNode, 'hidden', 'bounceInLeft');

        teaserHeaderNode[teaserHeaderNode.textContent ? 'textContent' : 'innerText'] = route.title;
        teaserHeaderNode.setAttribute('href', route.url);

        teaserTextNode.innerHTML   = route.teaser;

        setTimeout(function() {
            domClass.replace(teaserNode, 'bounceInLeft', 'hidden');
        }, 500);
    } // setTeaser()


    function setGallery() {
        domClass.replace(teaserGalleryNode, 'hidden', 'bounceInRight');

        teaserGalleryListNode.innerHTML = '';

        var data = currentCatalogRoute.data;
        var fragment   = document.createDocumentFragment();
        var imgData, imgSrc, thumb, imgTitle = null, insertedImgNode;

        for (var i = 0; i < data.images.length; i++) {
            imgData = data.images[i];

            if ({}.toString.call(imgData) === '[object Object]') {
                imgSrc = imgData.src;
                thumb = imgData.thumb ? imgData.thumb : imgSrc;
                if (imgData.title) imgTitle = imgData.title;
            }
            else {
                imgSrc = thumb = imgData;
            }

            insertedImgNode = put(fragment, 'li>a[href="$"]>img[src=$][alt=$]', imgSrc, thumb, data.title);
            imgTitle && put(insertedImgNode, '[title=$]', imgTitle);

            imgData = imgSrc = thumb = imgTitle = insertedImgNode = null;
        }

        put(teaserGalleryListNode, fragment);
        teaserGalleryHeaderNode.setAttribute('href', data.url + '#image-gallery');

        setTimeout(function() {
            domClass.replace(teaserGalleryNode, 'bounceInRight', 'hidden');
        }, 500);
    } // setGallery()


    function setNavigation() {
        var prevData, nextData;

        if (!currentCatalogRoute) {
            prevBtn.style.display = 'none';
            nextBtn.style.display = 'none';
            return;
        }

        prevData = currentCatalogRoute.data.prev;
        nextData = currentCatalogRoute.data.next;

        if (prevData) {
            prevBtn.setAttribute('href', location.pathname + '/#' + (prevData.url ? prevData.url.split('/').pop() : ''));
            prevBtn.setAttribute('title', prevData.title);
            prevBtn.style.display = '';
        } else {
            prevBtn.style.display = 'none';
        }

        if (nextData) {
            nextBtn.setAttribute('href', location.pathname + '/#' + (nextData.url ? nextData.url.split('/').pop() : ''));
            nextBtn.setAttribute('title', nextData.title);
            nextBtn.style.display = '';
        } else {
            nextBtn.style.display = 'none';
        }
    } // setNavigation()


    function setCurrentRoute(url) {
        var i, prevIdx, nextIdx;

        if (undefined === url) {
            currentCatalogRoute = null;
            return;
        }

        for (i = 0; i < routesData.length; i++) {
            if (routesData[i].url.split('/').pop() === url) {
                currentCatalogRoute.data = routesData[i];
                prevIdx = i - 1;
                nextIdx = i + 1;

                currentCatalogRoute.data.prev = undefined !== routesData[prevIdx] ? routesData[prevIdx] : null;
                currentCatalogRoute.data.next = undefined !== routesData[nextIdx] ? routesData[nextIdx] : null;

                break;
            }
        }
    } // setCurrentRouteData()

});