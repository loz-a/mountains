define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/Evented',
    'put-selector'

], function(declare, lang, Evented, put) { 'use strict';

    return declare(Evented, {

        constructor: function() {
            this.overlayNode  = null;
            this.expanderNode = null;
            this.contentNode  = null;
            this.closeBtnNode = null;
        },// constructor()


        // result template:
        // <div class="overlay hide">
        //    <div class="expander">
        //         <div class="content"></div>
        //        <div class="close">&times;</div>
        //     </div>
        // </div>
        createDOM: function() {
            this.overlayNode  = put(document.body, 'div.overlay.hide');
            this.expanderNode = put(this.overlayNode, 'div.expander');
            this.contentNode  = put(this.expanderNode, 'div.content');

            this.closeBtnNode = put(this.expanderNode, 'div.close', {
                onclick: lang.hitch(this, this.hide)
            });

            this.emit('createDOM');

        }, // createDOM()


        show: function() {
            this.overlayNode || this.createDOM();
            put(this.overlayNode, '!hide');
            this.emit('show');
        }, // show()


        hide: function() {
            if (!this.overlayNode) return;
            put(this.overlayNode, '.hide');
            this.emit('hide');
        } // hide()

    });

});