define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'put-selector',
    'dojo/on',
    'dojo/dom-class',
    'dojo/query',
    'widget/expander/Expander',
    './fullscreen'

], function(declare, lang, put, on, domClass, query, Expander, fullscreen) { 'use strict';

    var Gallery = declare([], {

        constructor: function(imagesContainer, options) {
            this.expander        = null,
            this.imagesList      = null;
            this.options         = lang.mixin({}, Gallery.DEFAULTS, options);

            this.imageViewerNode = null;
            this.imageNode       = null;
            this.captionNode     = null;
            this.nextBtnNode     = null;
            this.prevBtnNode     = null;

            on(imagesContainer, 'img:click', lang.hitch(this, function(evt) {
                this.show(evt.target);
                evt.preventDefault();
            }));
        }, // constructor()


        // result template:
        // <div class="image-viewer">
        //      <figure>
        //          <img src="" alt="">
        //          <figcaption></figcaption>
        //      </figure>
        // </div>
        createDOM: function() {
            this.imageViewerNode = put(this.expander.contentNode, 'div.image-viewer');
            this.imageNode       = put(this.imageViewerNode, 'figure > img[alt=""]', {onload: lang.hitch(this, this.onImageLoad)});
            this.captionNode     = put(this.imageNode, '+figcaption');
            this.nextBtnNode     = put(this.imageViewerNode, 'div.next>a[href="#"]', {innerHTML: '&rsaquo;'});
            this.prevBtnNode     = put(this.imageViewerNode, 'div.prev>a[href="#"]', {innerHTML: '&lsaquo;'});

            on(this.imageViewerNode, '.next a:click, .prev a:click', lang.hitch(this, this.onNavigateClick));

            on(window, 'resize', lang.hitch(this, function() {
                (!domClass.contains(this.expander.overlayNode, 'hide')) && this.adjustImage();
            }));

            if (fullscreen && Gallery.DEFAULTS.fullscreenEnable) {
                put(this.imageNode, '.zoom-in-cursor');

                on(this.imageNode, 'click', function(evt) {
                    var t = evt.target;
                    fullscreen.toggle(t.parentNode.parentNode);
                    fullscreen.isFullscreen ? put(t, '.zoom-out-cursor') : put(t, '!zoom-out-cursor');
                });
            }
        }, // createDOM()


        show: function(imgNode) {
            if (!this.expander) {
                this.expander   = new Expander();
            }
            this.expander.show();

            this.imageViewerNode || this.createDOM();

            if (!this.imagesList) {
                this.imagesList = new ImagesList(this);
                if (!this.options.viewImageList) {
                    this.imagesList.hide();
                }
            }

            this.setImage(imgNode);

            this.imagesList.replace(imgNode.parentNode.parentNode.parentNode);
            this.imagesList.active(imgNode);

            this.nextBtnNode.style.display = this.imagesList.next() ? '' : 'none';
            this.prevBtnNode.style.display = this.imagesList.prev() ? '' : 'none';
        }, // show()


        hide: function() {
            if (!this.expander) return;
            this.expander.hide();
        }, // hide()


        setImage: function(imgNode) {
            put(this.imageNode.parentNode, '.preload');

            this.imageNode.style.visibility = 'hidden';
            this.imageNode.src = imgNode.parentNode.getAttribute('href');

            this.setCaption(imgNode.hasAttribute('title') ? imgNode.getAttribute('title') : '');
        }, // setImage()


        setCaption: function(caption) {
            var cn = this.captionNode;

            cn.style.display = caption ? '' : 'none';
            cn[cn.textContent ? 'textContent' : 'innerText'] = caption;
        }, // setCaption()


        adjustImage: function() {

            this.imageNode.style.width  = '';
            this.imageNode.style.height = '';

            var imgWidth   = this.imageNode.offsetWidth;
            var imgHeight  = this.imageNode.offsetHeight;
            var avblWidth  = Math.floor(this.expander.expanderNode.clientWidth); // availableWidth
            var avblHeight = Math.floor(this.expander.expanderNode.clientHeight); // availableHeight
            var size;

            if (this.imagesList) {
                avblHeight = Math.floor(avblHeight - this.imagesList.imagesListNode.offsetHeight);
            }

            if (imgWidth >= imgHeight) {
                size = Gallery.resizeImage(imgWidth, imgHeight, avblWidth, avblHeight);
            } else if (imgHeight >= imgWidth) {
                size = Gallery.resizeImage(imgHeight, imgWidth, avblHeight, avblWidth);
                var height  = size.height;
                size.height = size.width;
                size.width  = height;
            }

            this.imageNode.style.width  = (size.width - 40) + 'px';
            this.imageNode.style.height = (size.height - 40) + 'px';
        }, // adjustImage()


        onNavigateClick: function(evt) {
            var parent    = evt.target.parentNode;
            var hasClass  = domClass.contains;
            var direction = hasClass(parent, 'next') ? 'next' : hasClass(parent, 'prev') ? 'prev' : null;

            if (!direction) return;

            var targetImg = this.imagesList[direction]();

            targetImg && this.imagesList.showImage(targetImg);

            evt.preventDefault();
        }, // onNavigateClick()


        onImageLoad: function() {
            var imgNode = this.imageNode;

            this.adjustImage();

            //put(this.expander.expanderNode, '!preload');
            put(imgNode, '.imageFadeIn');

            this.imageNode.style.visibility = '';

            setTimeout(function() {
                put(imgNode, '!imageFadeIn');
            }, 1200);
        } // onImageLoad()
    });


    Gallery.DEFAULTS = {
        'viewImageList': true,
        'fullscreenEnable': true
    }


    Gallery.resizeImage = function(imageWidth, imageHeight, maxWidth, maxHeight) {
        var width  = imageWidth,
            height = imageHeight;

        if (imageWidth > maxWidth) {
            width  = maxWidth;
            height = Math.floor(width / (imageWidth / imageHeight));
        }

        if (height > maxHeight) {
            var oldHeight = height;
            height = maxHeight;
            width  = Math.floor(height / (oldHeight / width));
        }

        return {
            height: height,
            width: width
        }
    }; // resizeImage()


    var ImagesList = declare([], {

        imageViewer: null,

        constructor: function(imageViewer) {
            this.imageViewer    = imageViewer;
            this.imagesListNode = null;
            this.activeImgNode  = null;

            this.createDOM();
        }, // constructor()


        // result template:
        // <div class="images-list">
        //      <ul></ul>
        // </div>
        createDOM: function() {
            this.imagesListNode  = put(this.imageViewer.expander.contentNode, 'div.images-list>ul');

            on(this.imagesListNode, 'img:click', lang.hitch(this, this.onImageClick));
        }, // createDOM()


        showImage: function(imgNode) {
            //this.imageViewer.imageNode.style.visibility = 'hidden';
            //this.imageViewer.imageNode.src = imgNode.parentNode.getAttribute('href');

            this.imageViewer.setImage(imgNode);
            this.active(imgNode);

            this.imageViewer.nextBtnNode.style.display = this.next() ? '' : 'none';
            this.imageViewer.prevBtnNode.style.display = this.prev() ? '' : 'none';
        }, // showImage()


        replace: function(donorContainer) {
            var fragment = document.createDocumentFragment();
            var children = donorContainer.children;
            var i;

            this.imagesListNode.innerHTML = '';

            for (i = 0; i < children.length; i++) {
                put(fragment, children[i].cloneNode(true));
            }
            put(this.imagesListNode, fragment);
        }, //replace()


        active: function(imgNode) {
            if (this.activeImgNode) {
                put(this.activeImgNode, '!active');
            }

            this.activeImgNode = query('a[href="' + imgNode.parentNode.getAttribute('href') + '"] img', this.imagesListNode)[0];
            put(this.activeImgNode, '.active');
        }, // active()


        hide: function() {
            this.imagesListNode.style.display = 'none';
        }, // hide()


        show: function() {
            this.imagesListNode.style.display = '';
        }, // show()


        next: function() {
            var next = this.activeImgNode.parentNode.parentNode.nextSibling;

            while(next && next.nodeType != 1) {
                next = next.nextSibling;
            }

            return next ? query('img', next)[0] : null;
        }, // next()


        prev: function() {
            var prev = this.activeImgNode.parentNode.parentNode.previousSibling;

            while(prev && prev.nodeType != 1) {
                prev = prev.nextSibling;
            }

            return prev ? query('img', prev)[0] : null;
        }, // prev()


        onImageClick: function(evt) {
            this.showImage(evt.target);
            evt.preventDefault();
        } // onImageClick()
    });

    return Gallery;
});