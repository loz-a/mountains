require([

    'widget/image-gallery/Gallery',
    'routes-slider'

], function(Gallery) { 'use strict';

    new Gallery(document.getElementById('image-gallery'));

});